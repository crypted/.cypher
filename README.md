Note: this repository is encrpyted with AES-256-GCM

a [cypher](../.cypher/objects) folder is decrypted directly into the .git/objects

we use the following command to mount the git repository :

```sh
cd $(git rev-parse --show-toplevel)
cryfs -c .secure/config.key .cypher/objects .git/objects -- -o nonempty
```

and we use the following to commit and push the encrypted repository

```sh
cd $(git rev-parse --show-toplevel)/.cypher
git add objects
git commit -a -m '...'
git push origin master

```


/!\ make sure the local encrypted repository doesn't have a remote !
